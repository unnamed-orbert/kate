# Translation of kategdbplugin to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2010, 2011, 2013, 2014, 2015.
# Kjetil Kilhavn <kjetil@kilhavn.no>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-22 00:39+0000\n"
"PO-Revision-Date: 2015-05-18 09:33+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, kde-format
msgid "GDB command"
msgstr "GDB-kommando"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "…"

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr "Søkestier til kildefil"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "Lokalt program"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "TCP til tjener"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "Serieport på tjener"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "Vert"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "Port"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "Baud"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr "solib-absolute-prefix"

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr "solib-search-path"

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr "Egendefinerte Init-kommandoer"

#: backend.cpp:24 backend.cpp:49 dapbackend.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""

#: configview.cpp:93
#, kde-format
msgid "Add new target"
msgstr "Legg til et nytt mål"

#: configview.cpp:97
#, kde-format
msgid "Copy target"
msgstr "Kopier mål"

#: configview.cpp:101
#, kde-format
msgid "Delete target"
msgstr "Slett mål"

#: configview.cpp:106
#, kde-format
msgid "Executable:"
msgstr "Programfil:"

#: configview.cpp:126
#, kde-format
msgid "Working Directory:"
msgstr "Arbeidsmappe:"

#: configview.cpp:134
#, kde-format
msgid "Process Id:"
msgstr ""

#: configview.cpp:139
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Argumenter:"

#: configview.cpp:142
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "Hold fokus"

#: configview.cpp:143
#, kde-format
msgid "Keep the focus on the command line"
msgstr "Hold fokus på kommandolinja"

#: configview.cpp:145
#, kde-format
msgid "Redirect IO"
msgstr "Omdiriger inn/utdata"

#: configview.cpp:146
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr "Omdiriger det feilsøkte programmets inn/ut-data til en egen fane"

#: configview.cpp:148
#, kde-format
msgid "Advanced Settings"
msgstr "Avanserte innstillinger"

#: configview.cpp:232
#, kde-format
msgid "Targets"
msgstr "Mål"

#: configview.cpp:525 configview.cpp:538
#, kde-format
msgid "Target %1"
msgstr "Mål %1"

#: dapbackend.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr ""

#: dapbackend.cpp:211
#, kde-format
msgid "program terminated"
msgstr ""

#: dapbackend.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr ""

#: dapbackend.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr ""

#: dapbackend.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr ""

#: dapbackend.cpp:270 gdbbackend.cpp:653
#, kde-format
msgid "stopped (%1)."
msgstr ""

#: dapbackend.cpp:278 gdbbackend.cpp:657
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr ""

#: dapbackend.cpp:280 gdbbackend.cpp:659
#, kde-format
msgid "Active thread: %1."
msgstr ""

#: dapbackend.cpp:285
#, kde-format
msgid "Breakpoint(s) reached:"
msgstr ""

#: dapbackend.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr ""

#: dapbackend.cpp:309
#, kde-format
msgid "all threads continued"
msgstr ""

#: dapbackend.cpp:316
#, kde-format
msgid "(running)"
msgstr ""

#: dapbackend.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr ""

#: dapbackend.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr ""

#: dapbackend.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr ""

#: dapbackend.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr ""

#: dapbackend.cpp:445
#, kde-format
msgid "important"
msgstr ""

#: dapbackend.cpp:448
#, kde-format
msgid "telemetry"
msgstr ""

#: dapbackend.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr ""

#: dapbackend.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr ""

#: dapbackend.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr ""

#: dapbackend.cpp:479
#, kde-format
msgid "thread %1"
msgstr ""

#: dapbackend.cpp:633
#, kde-format
msgid "breakpoint set"
msgstr ""

#: dapbackend.cpp:641
#, kde-format
msgid "breakpoint cleared"
msgstr ""

#: dapbackend.cpp:700
#, kde-format
msgid "(%1) breakpoint"
msgstr ""

#: dapbackend.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr ""

#: dapbackend.cpp:739
#, kde-format
msgid "server capabilities"
msgstr ""

#: dapbackend.cpp:742
#, kde-format
msgid "supported"
msgstr ""

#: dapbackend.cpp:742
#, kde-format
msgid "unsupported"
msgstr ""

#: dapbackend.cpp:745
#, kde-format
msgid "conditional breakpoints"
msgstr ""

#: dapbackend.cpp:746
#, kde-format
msgid "function breakpoints"
msgstr ""

#: dapbackend.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr ""

#: dapbackend.cpp:748
#, kde-format
msgid "log points"
msgstr ""

#: dapbackend.cpp:748
#, kde-format
msgid "modules request"
msgstr ""

#: dapbackend.cpp:749
#, kde-format
msgid "goto targets request"
msgstr ""

#: dapbackend.cpp:750
#, kde-format
msgid "terminate request"
msgstr ""

#: dapbackend.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr ""

#: dapbackend.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr ""

#: dapbackend.cpp:976 dapbackend.cpp:1011 dapbackend.cpp:1049
#: dapbackend.cpp:1083 dapbackend.cpp:1119 dapbackend.cpp:1155
#: dapbackend.cpp:1191 dapbackend.cpp:1291 dapbackend.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr ""

#: dapbackend.cpp:984 dapbackend.cpp:1019 dapbackend.cpp:1298
#: dapbackend.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr ""

#: dapbackend.cpp:991 dapbackend.cpp:996 dapbackend.cpp:1026
#: dapbackend.cpp:1031 dapbackend.cpp:1322 dapbackend.cpp:1327
#: dapbackend.cpp:1368 dapbackend.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr ""

#: dapbackend.cpp:1061 dapbackend.cpp:1095 dapbackend.cpp:1131
#: dapbackend.cpp:1167 dapbackend.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr ""

#: dapbackend.cpp:1067 dapbackend.cpp:1101 dapbackend.cpp:1137
#: dapbackend.cpp:1173 dapbackend.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr ""

#: dapbackend.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr ""

#: dapbackend.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr ""

#: dapbackend.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr ""

#: dapbackend.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr ""

#: dapbackend.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr ""

#: dapbackend.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr ""

#: dapbackend.cpp:1392 dapbackend.cpp:1399 dapbackend.cpp:1423
#, kde-format
msgid "none"
msgstr ""

#: dapbackend.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr ""

#: dapbackend.cpp:1402
#, kde-format
msgid "Session state: "
msgstr ""

#: dapbackend.cpp:1405
#, kde-format
msgid "initializing"
msgstr ""

#: dapbackend.cpp:1408
#, kde-format
msgid "running"
msgstr ""

#: dapbackend.cpp:1411
#, kde-format
msgid "stopped"
msgstr ""

#: dapbackend.cpp:1414
#, kde-format
msgid "terminated"
msgstr ""

#: dapbackend.cpp:1417
#, kde-format
msgid "disconnected"
msgstr ""

#: dapbackend.cpp:1420
#, kde-format
msgid "post mortem"
msgstr ""

#: dapbackend.cpp:1476
#, kde-format
msgid "command not found"
msgstr ""

#: dapbackend.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr ""

#: dapbackend.cpp:1605
#, kde-format
msgid "killing backend"
msgstr ""

#: dapbackend.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_1)
#: debugconfig.ui:33
#, kde-format
msgid "User Debug Adapter Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: debugconfig.ui:41
#, kde-format
msgid "Settings File:"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: debugconfig.ui:68
#, kde-format
msgid "Default Debug Adapter Settings"
msgstr ""

#: debugconfigpage.cpp:72 debugconfigpage.cpp:77
#, kde-format
msgid "Debugger"
msgstr ""

#: debugconfigpage.cpp:128
#, kde-format
msgid "No JSON data to validate."
msgstr ""

#: debugconfigpage.cpp:136
#, kde-format
msgid "JSON data is valid."
msgstr ""

#: debugconfigpage.cpp:138
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr ""

#: debugconfigpage.cpp:141
#, kde-format
msgid "JSON data is invalid: %1"
msgstr ""

#: gdbbackend.cpp:35
#, kde-format
msgid "Locals"
msgstr ""

#: gdbbackend.cpp:37
#, kde-format
msgid "CPU registers"
msgstr ""

#: gdbbackend.cpp:158
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr ""

#: gdbbackend.cpp:167
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""

#: gdbbackend.cpp:176
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""

#: gdbbackend.cpp:382
#, kde-format
msgid "Could not start debugger process"
msgstr "Klarte ikke starte feilsøkerprosessen"

#: gdbbackend.cpp:440
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** gdb avsluttet normalt ***"

#: gdbbackend.cpp:646
#, kde-format
msgid "all threads running"
msgstr ""

#: gdbbackend.cpp:648
#, kde-format
msgid "thread(s) running: %1"
msgstr ""

#: gdbbackend.cpp:678
#, kde-format
msgid "Current frame: %1:%2"
msgstr ""

#: gdbbackend.cpp:705
#, kde-format
msgid "Host: %1. Target: %1"
msgstr ""

#: gdbbackend.cpp:1375
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""

#: gdbbackend.cpp:1377
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr ""

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "Symbol"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "Verdi"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr ""

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr ""

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr ""

#: plugin_kategdb.cpp:103
#, kde-format
msgid "Kate Debug"
msgstr ""

#: plugin_kategdb.cpp:107
#, kde-format
msgid "Debug View"
msgstr "Feilsøkingsvisning"

#: plugin_kategdb.cpp:107 plugin_kategdb.cpp:340
#, kde-format
msgid "Debug"
msgstr "Feilsøk"

#: plugin_kategdb.cpp:110 plugin_kategdb.cpp:113
#, kde-format
msgid "Locals and Stack"
msgstr "Lokale og kallstabel"

#: plugin_kategdb.cpp:165
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "Nr"

#: plugin_kategdb.cpp:165
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Ramme"

#: plugin_kategdb.cpp:197
#, kde-format
msgctxt "Tab label"
msgid "Debug Output"
msgstr ""

#: plugin_kategdb.cpp:198
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Innstillinger"

#: plugin_kategdb.cpp:240
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""

#: plugin_kategdb.cpp:265
#, kde-format
msgid "Start Debugging"
msgstr "Start feilsøking"

#: plugin_kategdb.cpp:275
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "Drep / stopp feilsøking"

#: plugin_kategdb.cpp:282
#, kde-format
msgid "Continue"
msgstr "Fortsett"

#: plugin_kategdb.cpp:288
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "Slå av/på bruddpunkt/brudd"

#: plugin_kategdb.cpp:294
#, kde-format
msgid "Step In"
msgstr "Steg inn"

#: plugin_kategdb.cpp:301
#, kde-format
msgid "Step Over"
msgstr "Steg over"

#: plugin_kategdb.cpp:308
#, kde-format
msgid "Step Out"
msgstr "Steg ut"

#: plugin_kategdb.cpp:315 plugin_kategdb.cpp:347
#, kde-format
msgid "Run To Cursor"
msgstr "Kjør til markør"

#: plugin_kategdb.cpp:322
#, kde-format
msgid "Restart Debugging"
msgstr "Start feilsøking på nytt"

#: plugin_kategdb.cpp:330 plugin_kategdb.cpp:349
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "Flytt PC"

#: plugin_kategdb.cpp:335
#, kde-format
msgid "Print Value"
msgstr "Skriv ut verdi"

#: plugin_kategdb.cpp:344
#, kde-format
msgid "popup_breakpoint"
msgstr "oppsprett_bruddpunkt"

#: plugin_kategdb.cpp:346
#, kde-format
msgid "popup_run_to_cursor"
msgstr "oppsprett_kjør_til_markør"

#: plugin_kategdb.cpp:428 plugin_kategdb.cpp:444
#, kde-format
msgid "Insert breakpoint"
msgstr "Sett inn bruddpunkt"

#: plugin_kategdb.cpp:442
#, kde-format
msgid "Remove breakpoint"
msgstr "Fjern bruddpunkt"

#: plugin_kategdb.cpp:571
#, kde-format
msgid "Execution point"
msgstr "Kjøringspunkt"

#: plugin_kategdb.cpp:710
#, kde-format
msgid "Thread %1"
msgstr "Tråd %1"

#: plugin_kategdb.cpp:810
#, kde-format
msgid "IO"
msgstr "IO"

#: plugin_kategdb.cpp:894
#, kde-format
msgid "Breakpoint"
msgstr "Bruddpunkt"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "&Feilsøk"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, kde-format
msgid "Debug Plugin"
msgstr ""
